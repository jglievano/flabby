#!/usr/bin/python

"""Fetches information for prototype."""

import json
import urllib
import urllib2
import base64
from optparse import OptionParser

def fetchInformation(prototype):
  """Fetches json information for prototype."""
  req = urllib2.Request("http://localhost:3000/prototypes.json")
  response = urllib2.urlopen(req)
  json_string = response.read()
  json_data = json.loads(json_string)
  for p in json_data:
    if p['name'] == prototype:
      fetchPrototype(p['url'])

def fetchPrototype(url):
  """Fetch specific data for prototype."""
  req = urllib2.Request(url)
  response = urllib2.urlopen(req)
  json_string = response.read()
  json_data = json.loads(json_string)
  if json_data['build_number'] == None:
    update_json = {'build_number': 1, '_method': 'PUT'}
    update_json = json.dumps(update_json)
    update_req = urllib2.Request(url, update_json)
    base64string = base64.encodestring('admin:s3cr37.').replace('\n', '')
    update_req.add_header('Authorization', "Basic %s" % base64string)
    update_req.add_header('Content-Type', 'application/json')
    update_req.get_method = lambda: 'PUT'
    response = urllib2.urlopen(update_req)
    json_string = response.read()
    json_data['build_number'] = 1
  create_json = {'prototype_id': json_data['id'],
                 'version': '1.0.0',
                 'build_number': json_data['build_number'],
                 'public_release': False,
                 'description': 'No description',
                 '_method': 'POST'}
  create_json = json.dumps(create_json)
  create_req = urllib2.Request("http://localhost:3000/prototypes/%d/builds.json" % json_data['id'], create_json)
  base64string = base64.encodestring('admin:s3cr37.').replace('\n', '')
  create_req.add_header('Authorization', "Basic %s" % base64string)
  create_req.add_header('Content-Type', 'application/json')
  create_req.get_method = lambda: 'POST'
  response = urllib2.urlopen(create_req)
  print response.read()

def main():
  """Entry point."""
  parser = OptionParser("usage: %prog [options]")
  parser.add_option(
    "-p", "--prototype",
    dest="prototype",
    type="string",
    default="undefined",
    help="name of the prototype.")
  options = parser.parse_args()[0]
  fetchInformation(options.prototype)

if __name__ == "__main__":
  main()
