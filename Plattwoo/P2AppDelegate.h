//
//  P2AppDelegate.h
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/9/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <UIKit/UIKit.h>

BOOL isGameCenterAvailable();

@interface P2AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
