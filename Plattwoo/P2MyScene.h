//
//  P2MyScene.h
//  Plattwoo
//

//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface P2MyScene : SKScene

@property (nonatomic, assign) BOOL isPlaying;

@end
