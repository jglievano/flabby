//
//  P2ViewController.h
//  Plattwoo
//

//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GameCenterManager.h"
#import <iAd/iAd.h>
#import <SpriteKit/SpriteKit.h>

@interface P2ViewController : UIViewController
<ADBannerViewDelegate, GameCenterManagerDelegate>

@property (nonatomic, retain) ADBannerView *adBannerView;
@property (nonatomic, assign) BOOL adBannerViewIsVisible;

- (void)showAd;
- (void)hideAd;

- (void)showLeaderboard;

@end
