//
//  P2MyScene.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/9/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "P2MyScene.h"

#import "Hero.h"
#import "Damsel.h"
#import "P2ViewController.h"
#import "P2Obstacle.h"

#import "LocalyticsSession.h"
#import <AVFoundation/AVFoundation.h>
#import <GameKit/GameKit.h>

@implementation P2MyScene {
  NSUInteger _numberOfTaps;
  NSTimeInterval _previousTime;
  Hero *_hero;
  Damsel *_damsel;
  NSMutableArray *_tubes;
  SKSpriteNode *_backgroundFirst;
  SKSpriteNode *_backgroundSecond;

  SKLabelNode *_title;
  int _score;
  SKLabelNode *_scoreLabel;
  SKLabelNode *_recordLabel;
  SKLabelNode *_tapToPlay;
  SKSpriteNode *_leaderboardButton;
  SKSpriteNode *_rateUsButton;
  SKSpriteNode *_scoreboard;
  AVAudioPlayer *_backgroundMusicPlayer;

  SKAction *_swimAction;
  SKAction *_dieAction;

  BOOL _waitingToStart;
  BOOL _gameOver;
}

static float kDistanceBetweenTubes = 180;
static float kTubeSpeed = 115.0;
static float kInGameGravity = -4.9;
static float kJumpInitialVelocity = 290.0;
static NSString * const kStrGameFontName = @"BlackHawk";
static int kBigFontSize = 36;
static NSString * const kStrSwimAction = @"Swim";

-(id)initWithSize:(CGSize)size {    
  if (self = [super initWithSize:size]) {
    _isPlaying = YES;
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"only"
                                                              ofType:@"m4a"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    NSError *error;
    _backgroundMusicPlayer = [[AVAudioPlayer alloc]
        initWithContentsOfURL:soundFileURL error:&error];
    _backgroundMusicPlayer.numberOfLoops = -1;
    [_backgroundMusicPlayer prepareToPlay];
    [_backgroundMusicPlayer play];

    _waitingToStart = YES;

    _title = [SKLabelNode labelNodeWithFontNamed:kStrGameFontName];
    _title.text = @"A FISH JOURNEY";
    _title.fontSize = kBigFontSize;
    _title.position = CGPointMake(CGRectGetMidX(self.frame),
                                 CGRectGetMidY(self.frame) + 50);
    _title.zPosition = 99;
    [self addChild:_title];

    _tapToPlay = [SKLabelNode labelNodeWithFontNamed:kStrGameFontName];
    _tapToPlay.text = @"tap to play";
    _tapToPlay.fontSize = kBigFontSize;
    _tapToPlay.position = CGPointMake(CGRectGetMidX(self.frame),
                                  CGRectGetMidY(self.frame) - 80);
    _tapToPlay.zPosition = 99;
    [self addChild:_tapToPlay];

    _scoreboard = [SKSpriteNode spriteNodeWithImageNamed:@"scoreboard"];
    _scoreboard.position = CGPointMake(CGRectGetMidX(self.frame) - 10,
                                       CGRectGetMidY(self.frame) + 12);
    _scoreboard.zPosition = 98;
    _scoreboard.alpha = 0;
    [self addChild:_scoreboard];

    _scoreLabel = [SKLabelNode labelNodeWithFontNamed:kStrGameFontName];
    _scoreLabel.text = [NSString stringWithFormat:@"%d", _score];
    _scoreLabel.fontSize = kBigFontSize;
    _scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                       CGRectGetMaxY(self.frame) - 80);
    _scoreLabel.zPosition = 99;
    _scoreLabel.alpha = 0;
    [self addChild:_scoreLabel];

    _recordLabel = [SKLabelNode labelNodeWithFontNamed:kStrGameFontName];
    _recordLabel.text = [NSString stringWithFormat:@"%d", _score];
    _recordLabel.fontSize = kBigFontSize;
    _recordLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                       CGRectGetMidY(self.frame) - 40);
    _recordLabel.zPosition = 99;
    _recordLabel.alpha = 0;
    [self addChild:_recordLabel];

    _tubes = [NSMutableArray array];

    self.backgroundColor = [SKColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsWorld.gravity = CGVectorMake(0, 0.0);
    
    _backgroundFirst = [SKSpriteNode spriteNodeWithImageNamed:@"bg"];
    _backgroundFirst.anchorPoint = CGPointZero;
    _backgroundFirst.position = CGPointZero;
    [self addChild:_backgroundFirst];

    _backgroundSecond = [SKSpriteNode spriteNodeWithImageNamed:@"bg"];
    _backgroundSecond.anchorPoint = CGPointZero;
    _backgroundSecond.position = CGPointMake(_backgroundFirst.size.width - 1.0, 0);
    [self addChild:_backgroundSecond];

    _damsel = [[Damsel alloc] init];
    _damsel.xScale = 0.6;
    _damsel.yScale = 0.6;
    _damsel.position = CGPointMake(CGRectGetMaxX(self.frame) + _damsel.size.width,
                                   CGRectGetMidY(self.frame));
    _damsel.zPosition = 1000.0;
    [self addChild:_damsel];

    _hero = [[Hero alloc] init];
    _hero.position = CGPointMake(CGRectGetMidX(self.frame),
                                 CGRectGetMidY(self.frame));
    [self addChild:_hero];

    NSString *seaMagicPath = [[NSBundle mainBundle] pathForResource:@"bubble_pfx"
                                                             ofType:@"sks"];
    SKEmitterNode *seaMagic = [NSKeyedUnarchiver unarchiveObjectWithFile:seaMagicPath];
    seaMagic.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:seaMagic];

    NSString *fishesPath = [[NSBundle mainBundle] pathForResource:@"bob_pfx"
                                                           ofType:@"sks"];
    SKEmitterNode *fishes = [NSKeyedUnarchiver unarchiveObjectWithFile:fishesPath];
    fishes.position = CGPointMake(CGRectGetMinX(self.frame) - 40, CGRectGetMinY(self.frame) + 80);
    [self addChild:fishes];
    NSString *squidsPath = [[NSBundle mainBundle] pathForResource:@"squid_pfx"
                                                           ofType:@"sks"];
    SKEmitterNode *squids = [NSKeyedUnarchiver unarchiveObjectWithFile:squidsPath];
    squids.position = CGPointMake(CGRectGetMinX(self.frame) - 40, CGRectGetMinY(self.frame) + 120);
    [self addChild:squids];
    NSString *bluefishesPath = [[NSBundle mainBundle] pathForResource:@"dora_pfx"
                                                               ofType:@"sks"];
    SKEmitterNode *bluefishes = [NSKeyedUnarchiver unarchiveObjectWithFile:bluefishesPath];
    bluefishes.position = CGPointMake(CGRectGetMaxX(self.frame) + 40, CGRectGetMinY(self.frame) + 80);
    [self addChild:bluefishes];
    NSString *horsefishesPath = [[NSBundle mainBundle] pathForResource:@"seahorse_pfx"
                                                                ofType:@"sks"];
    SKEmitterNode *horsefishes = [NSKeyedUnarchiver unarchiveObjectWithFile:horsefishesPath];
    horsefishes.position = CGPointMake(CGRectGetMaxX(self.frame) + 40, CGRectGetMinY(self.frame) + 120);
    [self addChild:horsefishes];

    _leaderboardButton = [SKSpriteNode spriteNodeWithImageNamed:@"leaderboard_btn"];
    _leaderboardButton.position =
        CGPointMake(CGRectGetMidX(self.frame),
                    CGRectGetMinY(self.frame) + _leaderboardButton.size.height);
    _leaderboardButton.zPosition = 200;
    [self addChild:_leaderboardButton];

    _rateUsButton = [SKSpriteNode spriteNodeWithImageNamed:@"rate_btn"];
    _rateUsButton.position =
        CGPointMake(CGRectGetMidX(self.frame),
                    CGRectGetMinY(self.frame) + _rateUsButton.size.height * 2.1);
    _rateUsButton.zPosition = 200;
    [self addChild:_rateUsButton];
  }
  return self;
}

- (void)gameOver {
  [self storeRecord];
  _damsel.isLeaving = YES;
  [_hero die];
  [[LocalyticsSession shared] tagEvent:@"Game Over"
                            attributes:@{@"taps" : @(_numberOfTaps),
                                         @"score" : @(_score)}];
  self.physicsWorld.gravity = CGVectorMake(0, -10.0);
  if (_damsel.currentLevel == 0) {
    _title.text = @"";
  } else {
    [_damsel startLevel:(_damsel.currentLevel - 1)];
    _title.text = _damsel.name;
    _damsel.xScale = 1.0;
    _damsel.yScale = 1.0;
    _damsel.position = CGPointMake(CGRectGetMinX(_scoreboard.frame) + _damsel.size.width / 4 + 5,
                                   CGRectGetMaxY(_scoreboard.frame) - 18);
  }
  _title.colorBlendFactor = 0.0;
  _title.fontColor = [SKColor colorWithRed:0.99 green:0.8 blue:0.04 alpha:1];

  _scoreLabel.text = [NSString stringWithFormat:@"LVL %d", _score];
  _scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
  _scoreLabel.alpha = 0;
  [_scoreLabel runAction:[SKAction fadeInWithDuration:1.0]];
  [_title runAction:[SKAction fadeInWithDuration:1.0]];
  _recordLabel.text = [NSString stringWithFormat:@"TOP LVL %@",
      [[NSUserDefaults standardUserDefaults] objectForKey:@"record"]];
  P2ViewController *vc = (P2ViewController *)self.view.window.rootViewController;
  [vc showAd];
  [_scoreboard runAction:[SKAction fadeInWithDuration:1.0]];
  [_recordLabel runAction:[SKAction fadeInWithDuration:1.0]];
  [_rateUsButton runAction:[SKAction fadeInWithDuration:1.0]];
  [_leaderboardButton runAction:[SKAction fadeInWithDuration:1.0] completion:^{
    _waitingToStart = YES;
  }];
  _waitingToStart = NO;
  _gameOver = YES;
}

- (void)resetGame {
  _numberOfTaps = 0;
  _score = 0;
  _gameOver = NO;
  _hero.position = CGPointMake(CGRectGetMidX(self.frame),
                               CGRectGetMidY(self.frame));
  [_hero swim];
  [self removeChildrenInArray:_tubes];
  [_tubes removeAllObjects];
  P2ViewController *vc = (P2ViewController *)self.view.window.rootViewController;
  [vc hideAd];
  [_damsel runAction:[SKAction moveTo:CGPointMake(CGRectGetMaxX(self.frame) + _damsel.size.width,
                                                  CGRectGetMidY(self.frame))
                              duration:0.5]
          completion:^{
    _damsel.isLeaving = NO;
    [_damsel startLevel:0];
    _damsel.xScale = 0.6;
    _damsel.yScale = 0.6;
  }];
}

- (void)gotoReviews {
  NSString *str = @"itmss://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=822595437&mt=8";
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  if (!_isPlaying) {
    _isPlaying = YES;
  }
  UITouch *touch = [touches anyObject];
  if ([touch.view isKindOfClass:[ADBannerView class]]) {
    self.isPlaying = NO;
    self.paused = YES;
    return;
  }
  CGPoint location = [touch locationInNode:self];
  if (_gameOver) {
    if (_waitingToStart) {
      SKNode *node = [self nodeAtPoint:location];
      if (node == _leaderboardButton) {
        P2ViewController *vc = (P2ViewController *)self.view.window.rootViewController;
        [vc showLeaderboard];
        return;
      } else if (node == _rateUsButton) {
        [self gotoReviews];
        return;
      } else {
        [self resetGame];
        self.physicsWorld.gravity = CGVectorMake(0, kInGameGravity);
        [_scoreLabel runAction:[SKAction fadeOutWithDuration:1.0]];
        [_scoreboard runAction:[SKAction fadeOutWithDuration:1.0]];
        [_recordLabel runAction:[SKAction fadeOutWithDuration:1.0]];
        [_rateUsButton runAction:[SKAction fadeOutWithDuration:1.0]];
        [_leaderboardButton runAction:[SKAction fadeOutWithDuration:1.0]];
        [_title runAction:[SKAction fadeOutWithDuration:1.0] completion:^{
          _scoreLabel.text = [NSString stringWithFormat:@"%d", _score];
          _scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                             CGRectGetMaxY(self.frame) - 80);
          _scoreLabel.alpha = 0;
          [_scoreLabel runAction:[SKAction fadeInWithDuration:1.0]];
        }];
        _waitingToStart = NO;
      }
    }
    return;
  }
  if (_waitingToStart) {
    SKNode *node = [self nodeAtPoint:location];
    if (node == _leaderboardButton) {
      P2ViewController *vc = (P2ViewController *)self.view.window.rootViewController;
      [vc showLeaderboard];
      return;
    } else if (node == _rateUsButton) {
      [self gotoReviews];
      return;
    } else {
      self.physicsWorld.gravity = CGVectorMake(0, kInGameGravity);
      _waitingToStart = NO;
      [_scoreLabel runAction:[SKAction fadeOutWithDuration:1.0]];
      [_tapToPlay runAction:[SKAction fadeOutWithDuration:1.0]];
      [_rateUsButton runAction:[SKAction fadeOutWithDuration:1.0]];
      [_leaderboardButton runAction:[SKAction fadeOutWithDuration:1.0]];
      [_title runAction:[SKAction fadeOutWithDuration:1.0] completion:^{
        _scoreLabel.alpha = 0;
        _scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                           CGRectGetMaxY(self.frame) - 80);
        [_scoreLabel runAction:[SKAction fadeInWithDuration:1.0]];
      }];
      return;
    }
  }
  _numberOfTaps++;
  _hero.physicsBody.velocity = CGVectorMake(0, kJumpInitialVelocity);
}

- (void)storeRecord {
  NSNumber *record = [[NSUserDefaults standardUserDefaults] objectForKey:@"record"];
  [[GameCenterManager sharedManager] saveAndReportScore:_score
                                            leaderboard:@"com.eosch.plattwoo.scores.general"
                                              sortOrder:GameCenterSortOrderHighToLow];
  if (!record || [record intValue] < _score) {
    [[LocalyticsSession shared] tagEvent:@"topped record"];
    [[NSUserDefaults standardUserDefaults] setObject:@(_score) forKey:@"record"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
}

- (void)update:(CFTimeInterval)currentTime {
  if (!_isPlaying) {
    _previousTime = 0;
    return;
  }
  float deltaTime = 0;
  if (_previousTime > 0) {
    deltaTime = currentTime - _previousTime;
  }
  _previousTime = currentTime;
  // First, update the hero.
  SKAction *rotate = [SKAction rotateToAngle:(_hero.physicsBody.velocity.dy / 200) duration:0.1];
  [_hero runAction:rotate];
  _hero.zPosition = 100;

  if (_gameOver) {
    return;
  }

  // Second, update the background.
  _backgroundFirst.position = CGPointMake(_backgroundFirst.position.x - 20 * deltaTime,
                                          _backgroundFirst.position.y);
  _backgroundSecond.position = CGPointMake(_backgroundSecond.position.x - 20 * deltaTime,
                                           _backgroundSecond.position.y);
  if (_backgroundFirst.position.x < -_backgroundFirst.size.width) {
    float diff = fabsf(_backgroundFirst.size.width);
    _backgroundFirst.position =
        CGPointMake(_backgroundFirst.position.x + diff,
                    _backgroundFirst.position.y);
    _backgroundSecond.position =
        CGPointMake(_backgroundSecond.position.x + diff,
                    _backgroundSecond.position.y);
  }

  if (_waitingToStart) {
    return;
  }

  // Third, update the tubes.
  float furthestTubeX = CGRectGetMaxX(self.frame);
  NSMutableArray *tubesToBeRemoved = [NSMutableArray array];
  for (P2Obstacle *tube in _tubes) {
    tube.position = CGPointMake(tube.position.x - kTubeSpeed * deltaTime, tube.position.y);
    if (tube.position.x < CGRectGetMinX(self.frame) - tube.size.width) {
      [tubesToBeRemoved addObject:tube];
    } else {
      if (tube.position.x > furthestTubeX) {
        furthestTubeX = tube.position.x;
      } else {
        if (tube.position.x < CGRectGetMidX(self.frame) && !tube.hasPassed) {
          tube.hasPassed = YES;
          _score++;
          if (_damsel.position.x - _damsel.size.width / 2 < _hero.position.x &&
              _damsel.currentLevel < 4) {
            [_hero swim];
            _damsel.isLeaving = YES;
            [_damsel runAction:[SKAction moveToX:CGRectGetMaxX(self.frame) + _damsel.size.width
                                        duration:0.5]
                    completion:^{
              _damsel.isLeaving = NO;
              [_damsel startLevel:(_damsel.currentLevel + 1)];
            }];
          }
          _scoreLabel.text = [NSString stringWithFormat:@"%d", _score];
          [self runAction:[SKAction playSoundFileNamed:@"passed_tube.mp3" waitForCompletion:NO]];
        }
        CGRect heroRect = CGRectInset(_hero.frame, 15, 15);
        if (CGRectIntersectsRect(heroRect, tube.frame)) {
          [self gameOver];
          return;
        }
      }
    }
  }
  [self removeChildrenInArray:tubesToBeRemoved];
  [_tubes removeObjectsInArray:tubesToBeRemoved];

  // Fourth, create new tubes.
  while (_tubes.count < 10) {
    float yOffset = 110 - (int)(arc4random() % 220);
    NSArray *tubeNames = @[@"tube_1", @"tube_2"];
    P2Obstacle *bottomTube = [P2Obstacle spriteNodeWithImageNamed:
        [tubeNames objectAtIndex:(arc4random() % tubeNames.count)]];
    bottomTube.position = CGPointMake(furthestTubeX + kDistanceBetweenTubes,
                                      CGRectGetMinY(self.frame) + yOffset);
    bottomTube.hasPassed = YES;
    [self addChild:bottomTube];
    [_tubes addObject:bottomTube];
    P2Obstacle *topTube = [P2Obstacle spriteNodeWithImageNamed:
        [tubeNames objectAtIndex:(arc4random() % tubeNames.count)]];
    topTube.position = CGPointMake(furthestTubeX + kDistanceBetweenTubes,
                                   550 + yOffset);
    topTube.yScale = -1.0;
    [self addChild:topTube];
    [_tubes addObject:topTube];

    furthestTubeX += kDistanceBetweenTubes;
  }

  // Fifth, update damsel.
  if (_damsel.currentLevel < 4) {
    _damsel.position =
        CGPointMake(_damsel.position.x - (5 + (1 * 5 / (_damsel.currentLevel + 1))) * deltaTime,
                    _damsel.position.y);
    if (!_damsel.isLeaving && _damsel.position.x < CGRectGetMaxX(self.frame) && !_hero.isInLove) {
      [_hero inlove];
    }
  }
}

@end
