//
//  P2Obstacle.h
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/15/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface P2Obstacle : SKSpriteNode

@property (nonatomic, assign) BOOL hasPassed;

@end
