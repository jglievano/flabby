//
//  GameObject.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/17/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "GameObject.h"

@implementation GameObject {
  NSMutableDictionary *_actions;
  NSString *_actionFormat;
}

static NSString * const kStrActionsKey = @"actions";
static NSString * const kStrNameKey = @"name";
static NSString * const kStrFramesNumKey = @"framesNum";
static NSString * const kStrFormatKey = @"format";
static NSString * const kStrFrameRateKey = @"frameRate";
static NSString * const kStrRepeatableKey = @"repeatable";

- (id)initWithName:(NSString *)name
            format:(NSString *)format
   initializerName:(NSString *)initializer {
  self = [super initWithImageNamed:initializer];
  if (self) {
    _actions = [NSMutableDictionary dictionary];
    _actionFormat = format;

    NSString *specsPath = [[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    NSDictionary *specs = [NSDictionary dictionaryWithContentsOfFile:specsPath];
    NSArray *actions = [specs objectForKey:kStrActionsKey];
    for (NSDictionary *action in actions) {
      NSString *name = [action objectForKey:kStrNameKey];
      NSNumber *framesNum = [action objectForKey:kStrFramesNumKey];
      NSString *assetFormat = [action objectForKey:kStrFormatKey];
      NSMutableArray *frames = [NSMutableArray array];
      for (unsigned int i = 0; i < [framesNum intValue]; i++) {
        NSString *assetName = [NSString stringWithFormat:assetFormat, (i + 1)];
        SKTexture *asset = [SKTexture textureWithImageNamed:assetName];
        [frames addObject:asset];
      }
      NSNumber *repeatable = [action objectForKey:kStrRepeatableKey];
      NSNumber *frameRate = [action objectForKey:kStrFrameRateKey];
      SKAction *baseAction = [SKAction animateWithTextures:frames
                                              timePerFrame:[frameRate doubleValue]];
      if ([repeatable boolValue]) {
        SKAction *repeatableAction = [SKAction repeatActionForever:baseAction];
        [_actions setObject:repeatableAction
                     forKey:[NSString stringWithFormat:_actionFormat, name]];
      } else {
        [_actions setObject:baseAction
                     forKey:[NSString stringWithFormat:_actionFormat, name]];
      }
    }
  }
  return self;
}

- (SKAction *)objectActionForKey:(NSString *)key {
  SKAction *action = [_actions objectForKey:[NSString stringWithFormat:_actionFormat, key]];
  return action;
}

- (void)startActionForKey:(NSString *)key {
  SKAction *action = [_actions objectForKey:[NSString stringWithFormat:_actionFormat, key]];
  [self runAction:action withKey:[NSString stringWithFormat:_actionFormat, key]];
}

- (void)stopActionForKey:(NSString *)key {
  [self removeActionForKey:[_actions objectForKey:[NSString stringWithFormat:_actionFormat, key]]];
}

@end
