//
//  ParticleFX.h
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/17/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface ParticleFX : SKEmitterNode

- (id)initWithName:(NSString *)name;

@end
