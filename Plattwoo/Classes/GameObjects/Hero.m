//
//  Hero.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/17/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "Hero.h"

@implementation Hero

static NSString * const kStrActionFormat = @"com.eosch.plattwoo.actions.hero.%@";
static NSString * const kStrName = @"hero";
static NSString * const kStrInitializer = @"fish_swim_1";
// These two MUST be the same name specified in hero.plist.
static NSString * const kStrSwimAction = @"swim";
static NSString * const kStrDieAction = @"die";
static NSString * const kStrInLoveAction = @"inlove";

- (id)init {
  self = [super initWithName:kStrName format:kStrActionFormat initializerName:kStrInitializer];
  if (self) {
    self.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.size.width / 2];
    self.physicsBody.dynamic = YES;
    [self swim];
  }
  return self;
}

- (void)swim {
  _isInLove = NO;
  [self removeAllActions];
  [self startActionForKey:kStrSwimAction];
}

- (void)die {
  _isInLove = NO;
  [self removeAllActions];
  [self startActionForKey:kStrDieAction];
}

- (void)inlove {
  _isInLove = YES;
  [self removeAllActions];
  [self startActionForKey:kStrInLoveAction];
}

@end
