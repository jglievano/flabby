//
//  Damsel.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 3/2/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "Damsel.h"

@implementation Damsel

static NSString * const kStrActionFormat = @"com.eosch.plattwoo.actions.damsel.%@";
static NSString * const kStrName = @"damsel";
static NSString * const kStrInitializer = @"hot_fish_1";
// These two MUST be the same name specified in damsel.plist.
static NSString * const kStrLvl1Action = @"hot";
static NSString * const kStrLvl2Action = @"egypt";
static NSString * const kStrLvl3Action = @"asian";
static NSString * const kStrLvl4Action = @"queen";

static NSString * const kStrLvl1Name = @"KELLY";
static NSString * const kStrLvl2Name = @"MIU";
static NSString * const kStrLvl3Name = @"LEI";
static NSString * const kStrLvl4Name = @"ANGIE";

- (id)init {
  self = [super initWithName:kStrName format:kStrActionFormat initializerName:kStrInitializer];
  if (self) {
    [self startLevel:0];
  }
  return self;
}

- (NSString *)name {
  switch (_currentLevel) {
    case 0:
      return kStrLvl1Name;
    case 1:
      return kStrLvl2Name;
    case 2:
      return kStrLvl3Name;
    case 3:
      return kStrLvl4Name;

    default:
      break;
  }
  return @"";
}

- (void)startLevel:(int)level {
  _currentLevel = level;
  [self removeAllActions];
  switch (level) {
    case 0:
      [self startActionForKey:kStrLvl1Action];
      break;
    case 1:
      [self startActionForKey:kStrLvl2Action];
      break;
    case 2:
      [self startActionForKey:kStrLvl3Action];
      break;
    case 3:
      [self startActionForKey:kStrLvl4Action];
      break;

    default:
      break;
  }
}

@end
