//
//  GameObject.h
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/17/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameObject : SKSpriteNode

- (id)initWithName:(NSString *)name
            format:(NSString *)format
   initializerName:(NSString *)initializer;
- (SKAction *)objectActionForKey:(NSString *)key;
- (void)startActionForKey:(NSString *)key;
- (void)stopActionForKey:(NSString *)key;

@end
