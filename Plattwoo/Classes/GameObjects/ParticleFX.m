//
//  ParticleFX.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/17/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "ParticleFX.h"

@implementation ParticleFX

- (id)initWithName:(NSString *)name {
  NSString *path = [[NSBundle mainBundle] pathForResource:name
                                                   ofType:@"sks"];
  SKEmitterNode *emitter = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
  self = (ParticleFX *)emitter;
  if (self) {

    SKEmitterNode *emitter = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    self = (ParticleFX *)emitter;
  }
  return self;
}

@end
