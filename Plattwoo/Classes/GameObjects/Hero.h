//
//  Hero.h
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/17/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "GameObject.h"

@interface Hero : GameObject

@property (nonatomic, readonly) BOOL isInLove;

- (void)swim;
- (void)die;
- (void)inlove;

@end
