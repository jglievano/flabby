//
//  Damsel.h
//  Plattwoo
//
//  Created by Gabriel Liévano on 3/2/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "GameObject.h"

@interface Damsel : GameObject

@property (nonatomic, assign) BOOL isLeaving;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) int currentLevel;

- (void)startLevel:(int)level;

@end
