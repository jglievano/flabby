 //
//  main.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/9/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "P2AppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([P2AppDelegate class]));
  }
}
