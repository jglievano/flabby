//
//  P2AppDelegate.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/9/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "P2AppDelegate.h"

#import "P2MyScene.h"
#import "P2ViewController.h"

#import "GameCenterManager.h"
#import "LocalyticsSession.h"

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define LOCALYTICS_KEY @ STRINGIZE2(LOCALYTICS)

@implementation P2AppDelegate {
  P2ViewController *_viewController;
}

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  // Override point for customization after application launch.
  [[GameCenterManager sharedManager] setupManager];
  _viewController = (P2ViewController *)self.window.rootViewController;
  [GameCenterManager sharedManager].delegate = _viewController;
  return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state. This can occur for
  // certain types of temporary interruptions (such as an incoming phone call or SMS message) or
  // when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame
  // rates. Games should use this method to pause the game.
  [[LocalyticsSession shared] close];
  [[LocalyticsSession shared] upload];
  SKView *view = (SKView *)self.window.rootViewController.view;
  ((P2MyScene *)(view.scene)).isPlaying = NO;
  view.paused = YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate timers, and store
  // enough application state information to restore your application to its current state in case
  // it is terminated later.
  // If your application supports background execution, this method is called instead of
  // applicationWillTerminate: when the user quits.
  [[LocalyticsSession shared] close];
  [[LocalyticsSession shared] upload];
  SKView *view = (SKView *)self.window.rootViewController.view;
  ((P2MyScene *)(view.scene)).isPlaying = NO;
  view.paused = YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the inactive state; here you can undo
  // many of the changes made on entering the background.
  [[LocalyticsSession shared] resume];
  [[LocalyticsSession shared] upload];
  SKView *view = (SKView *)self.window.rootViewController.view;
  view.paused = NO;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If
  // the application was previously in the background, optionally refresh the user interface.
  [[LocalyticsSession shared] LocalyticsSession:LOCALYTICS_KEY];
  [[LocalyticsSession shared] resume];
  [[LocalyticsSession shared] upload];
  SKView *view = (SKView *)self.window.rootViewController.view;
  view.paused = NO;
}

- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if appropriate. See also
  // applicationDidEnterBackground:.
  [[LocalyticsSession shared] close];
  [[LocalyticsSession shared] upload];
}

@end
