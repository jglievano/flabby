//
//  P2ViewController.m
//  Plattwoo
//
//  Created by Gabriel Liévano on 2/9/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "P2ViewController.h"
#import "P2MyScene.h"

#import "LocalyticsSession.h"

@implementation P2ViewController

- (void)showAd {
  _adBannerView.frame = CGRectMake(0, -50, self.view.frame.size.width, 50);
  if (_adBannerViewIsVisible) {
    [UIView animateWithDuration:1.0 animations:^{
      _adBannerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    }];
  }
}

- (void)hideAd {
  [UIView animateWithDuration:1.0
                        delay:2.0
                      options:UIViewAnimationOptionAllowUserInteraction
                   animations:^{
                     _adBannerView.frame = CGRectMake(0, -50, self.view.frame.size.width, 50);
                   }
                   completion:^(BOOL completed) {}];
  _adBannerView.frame = CGRectMake(0, -50, self.view.frame.size.width, 50);
}

- (void)createAdBannerView {
  self.adBannerView = [[ADBannerView alloc] initWithFrame:
      CGRectMake(0, -50, self.view.frame.size.width, 50)];
  _adBannerView.delegate = self;
  [self.view addSubview:_adBannerView];
}

- (void)viewDidLoad {
  [super viewDidLoad];

  // Configure the view.
  SKView * skView = (SKView *)self.view;
  skView.showsFPS = NO;
  skView.showsNodeCount = NO;
  
  // Create and configure the scene.
  SKScene * scene = [P2MyScene sceneWithSize:skView.bounds.size];
  scene.scaleMode = SKSceneScaleModeAspectFill;
  
  // Present the scene.
  [skView presentScene:scene];

  [self createAdBannerView];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
  if (!willLeave) {
    ((SKView *)self.view).paused = YES;
    P2MyScene *scene = (P2MyScene *)((SKView *)self.view).scene;
    scene.isPlaying = NO;
  }
  return YES;
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
  if (!_adBannerViewIsVisible) {
    _adBannerViewIsVisible = YES;
  }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
  if (_adBannerViewIsVisible) {
    _adBannerViewIsVisible = NO;
  }
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
  ((SKView *)self.view).paused = NO;
  P2MyScene *scene = (P2MyScene *)((SKView *)self.view).scene;
  scene.paused = NO;
  scene.isPlaying = YES;
}

- (BOOL)shouldAutorotate {
  return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    return UIInterfaceOrientationMaskAllButUpsideDown;
  } else {
    return UIInterfaceOrientationMaskAll;
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Release any cached data, images, etc that aren't in use.
}

- (void)showLeaderboard {
  [[LocalyticsSession shared] tagEvent:@"showing leaderboard"];
  [[GameCenterManager sharedManager] presentLeaderboardsOnViewController:self];
}

- (void)gameCenterManager:(GameCenterManager *)manager
         authenticateUser:(UIViewController *)gameCenterLoginController {
  [self presentViewController:gameCenterLoginController animated:YES completion:^{
    NSLog(@"Finished Presenting Authentication Controller");
  }];
}

@end
